import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import store from './store';
import App from './App';

test('renders learn react link', () => {
  render(
    <Provider
      store={store}
    >
      <App />
    </Provider>
  );
  const container = screen.getByTestId('container');
  const increment = screen.getByTestId('increment');
  const counter = screen.getByTestId('counter');
  const decrement = screen.getByTestId('decrement');

  expect(container).toBeInTheDocument();
  expect(increment).toBeInTheDocument();
  expect(counter).toBeInTheDocument();
  expect(decrement).toBeInTheDocument();

  expect(container).toHaveStyle('display: flex');
  expect(container).toHaveStyle('justify-content: center');
  expect(container).toHaveStyle('align-items: center');
  expect(container).toHaveStyle('background-color: #424242');
  expect(container).toHaveStyle('height: 100%');
  
  expect(counter).toHaveStyle('color: #b8329d');
  
  expect(increment).toHaveStyle('background-color: #8e46a4');
  expect(increment).toHaveStyle('color: white');
  expect(increment).toHaveStyle('border-radius: 5px');
  expect(increment).toHaveStyle('margin: 5px');
  expect(increment).toHaveStyle('padding: 10px');
  expect(increment).toHaveStyle('cursor: pointer');
  
  expect(decrement).toHaveStyle('background-color: #8e46a4');
  expect(decrement).toHaveStyle('color: white');
  expect(decrement).toHaveStyle('border-radius: 5px');
  expect(decrement).toHaveStyle('margin: 5px');
  expect(decrement).toHaveStyle('padding: 10px');
  expect(decrement).toHaveStyle('cursor: pointer');
});
