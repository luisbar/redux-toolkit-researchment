import { useDispatch, useSelector } from 'react-redux';
import { increment, decrement } from './counterSlice';

function App() {
  const dispatch = useDispatch();
  const count = useSelector(state => state.counter.value);

  return (
    <div
      data-testid='container'
      style={styles.container}
    >
      <button
        data-testid='increment'
        onClick={() => dispatch(increment())}
        style={styles.button}
      >
        +
      </button>
      <h1 
        data-testid='counter'
        style={styles.counter}
      >
        {count}
      </h1>
      <button
        data-testid='decrement'
        onClick={() => dispatch(decrement())}
        style={styles.button}
      >
        -
      </button>
    </div>
  );
}

const styles = {
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#424242',
    height: '100%',
  },
  counter: { color: '#b8329d' },
  button: {
    backgroundColor: '#8e46a4',
    color: 'white',
    borderRadius: '5px',
    margin: '5px',
    border: 'none',
    padding: 10,
    cursor: 'pointer',
  },
};

export default App;
