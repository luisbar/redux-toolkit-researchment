import { configureStore } from '@reduxjs/toolkit';
import couterReducer from './counterSlice';

export default configureStore({
  reducer: {
    counter: couterReducer,
  },
});